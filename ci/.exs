repositories_to_add

old code but repositories_to_add have to be recycled!

* modified, waiting for test result!

##################

require DockerFactory

ci_registry_image = System.fetch_env!("CI_REGISTRY_IMAGE")

# debian/sudo
DockerFactory.apt_init_and_sudo(
    nil, "debian",
    ci_registry_image, "sudo",
    push: true
)

# debian/flatpak
DockerFactory.apt_flatpak(
    ci_registry_image, "sudo",
    ci_registry_image, "flatpak",
    push: true
)

# debian/openrc
DockerFactory.apt_openrc(
    ci_registry_image, "sudo",
    ci_registry_image, "openrc",
    push: true
)

# debian/mariadb
DockerFactory.apt_mariadb(
    ci_registry_image, "openrc",
    ci_registry_image, "mariadb",
    push: true
)

# debian/postgresql
DockerFactory.apt_postgresql(
    ci_registry_image, "openrc",
    ci_registry_image, "postgresql",
    push: true
)

# debian/sqlite
DockerFactory.apt_sqlite(
    ci_registry_image, "openrc",
    ci_registry_image, "sqlite",
    push: true
)

# debian/php-mysql
DockerFactory.apt_php_pdo_mysql(
    ci_registry_image, "mariadb",
    ci_registry_image, "php-pdo-mysql",
    push: true
)

# debian/php-pgsql
DockerFactory.apt_php_pdo_pgsql(
    ci_registry_image, "postgresql",
    ci_registry_image, "php-pdo-pgsql",
    push: true
)

# debian/php-sqlite
DockerFactory.apt_php_pdo_sqlite(
    ci_registry_image, "sqlite",
    ci_registry_image, "php-pdo-sqlite",
    push: true
)

# debian/sudo:stable-slim
DockerFactory.apt_init_and_sudo(
    nil, "debian",
    ci_registry_image, "sudo",
    tag: "stable-slim",
    push: true
)

# debian/sudo:oldstable
DockerFactory.apt_init_and_sudo(
    nil, "debian",
    ci_registry_image, "sudo",
    tag: "oldstable",
    push: true
)

# debian/sudo:oldstable-slim
DockerFactory.apt_init_and_sudo(
    nil, "debian",
    ci_registry_image, "sudo",
    tag: "oldstable-slim",
    push: true
)

# debian/sudo:testing
DockerFactory.apt_init_and_sudo(
    nil, "debian",
    ci_registry_image, "sudo",
    tag: "testing",
    push: true,
    repositories_to_add: [ # !!!
        "deb http://ftp.debian.org/debian stable main contrib non-free"
    ]
)

# debian/sudo:testing-slim
DockerFactory.apt_init_and_sudo(
    nil, "debian",
    ci_registry_image, "sudo",
    tag: "testing-slim",
    push: true,
    repositories_to_add: [ # !!!
        "deb http://ftp.debian.org/debian stable main contrib non-free"
    ]
)

# debian/sudo:unstable
DockerFactory.apt_init_and_sudo(
    nil, "debian",
    ci_registry_image, "sudo",
    tag: "unstable",
    push: true,
    repositories_to_add: [ # !!!
        "deb http://ftp.debian.org/debian stable main contrib non-free",
        "deb http://ftp.debian.org/debian testing main contrib non-free"
    ]
)

# debian/sudo:unstable-slim
DockerFactory.apt_init_and_sudo(
    nil, "debian",
    ci_registry_image, "sudo",
    tag: "unstable-slim",
    push: true,
    repositories_to_add: [ # !!!
        "deb http://ftp.debian.org/debian stable main contrib non-free",
        "deb http://ftp.debian.org/debian testing main contrib non-free"
    ]
)


