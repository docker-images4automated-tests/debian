require DockerFactory

DockerFactory.make_debian_images(
    tag: "testing-slim",
    repositories_to_add: [
        "deb http://ftp.debian.org/debian stable main contrib non-free"
    ]
)
