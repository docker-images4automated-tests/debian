require DockerFactory

DockerFactory.make_debian_images(
    tag: "unstable-slim",
    repositories_to_add: [
        "deb http://ftp.debian.org/debian stable main contrib non-free",
        "deb http://ftp.debian.org/debian testing main contrib non-free"
    ]
)
