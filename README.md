# debian

## Docker images produced by this project from debian (size for 2019 version)
To be updated
* debian (114MB)
  * sudo (120MB)
    * ash_busybox (128MB)
    * openrc_easy (124MB)

## Notes
* gdebi-core added to ease installation of locally downloaded .deb files
  (installs also python3 as a dependency,
  removed from sudo: too many dependencies 100MB, it was not this one for the dependencies)
* software-properties-common installed (0.081MB - 0.457MB) to allow
  * add-apt-repository 'deb http://ftp.debian.org/debian stable main contrib non-free'
  * check dependencies disk usage!!!
* apt-utils installed to avoid a warning message without importance:
  debconf: delaying package configuration, since apt-utils is not installed
  (1.1MB)

## Auto-login
* [debian auto login](https://google.com/search?q=debian+auto+login)

## Unofficial documentation
* [*Using Runit in a Docker Container*
  ](https://sourcediver.org/blog/2014/11/17/using-runit-in-a-docker-container/)
  2014-11
* [*How to automatically restart a linux background process if it fails?*
  ](https://superuser.com/questions/507576/how-to-automatically-restart-a-linux-background-process-if-it-fails)
  2012-11